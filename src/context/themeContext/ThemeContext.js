import {useState, useContext, createContext} from 'react'

export const ThemeContext = createContext();

export function ThemeProvider(props) {
  const [themeType, setThemeType] = useState('card');

  return (
    <ThemeContext.Provider value={{ themeType, setThemeType }}>
      {props.children}
    </ThemeContext.Provider>
  )
}

export function useThemeType() {
  return useContext(ThemeContext);
}
