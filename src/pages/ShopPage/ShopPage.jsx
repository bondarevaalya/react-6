import React from "react";
import styles from "./ShopPage.module.scss";
import ShopItem from "../../components/ShopItem";
import ShopItemTable from "../../components/ShopItemTable"
import { useSelector } from "react-redux";
import { useThemeType } from "../../context/themeContext/ThemeContext";

function ShopPage() {
  const { themeType } = useThemeType();
  const goods = useSelector((store) => store.goods.items);

  return (
    <>
      {themeType === "card" && (
        <section className={styles.ShopPage}>
          {goods.length > 0 &&
            goods.map((e, index) => (
              <ShopItem
                {...e}
                key={e.id}
                index={index}
                shop={true}
                cart={false}
              />
            ))}
          {!goods.length && <img src="./loader.gif" alt="loading store" />}
        </section>
      )}
      {themeType === "table" && (
        <section style={{"display":"flex", "justifyContent":"center"}}>
          <table>
            <caption>Products</caption>
            <tbody>
            {goods.length > 0 &&
              goods.map((e, index) => (
                <ShopItemTable
                  {...e}
                  key={e.id}
                  index={index}
                  shop={true}
                  cart={false}
                />
              ))}
            {!goods.length && <img src="./loader.gif" alt="loading store" />}
            </tbody>
          </table>
        </section>
      )}
    </>
  );
}

export default ShopPage;
