import { NavLink } from "react-router-dom";
import styles from "./Header.module.scss";
import { useThemeType } from "../../context/themeContext/ThemeContext";
function Header() {
  const { setThemeType } = useThemeType();
  return (
    <>
      <header className={styles.Header}>
        <NavLink to="/"> Shop</NavLink>
        <NavLink to="/fav">Favorites</NavLink>
        <NavLink to="/cart">Cart</NavLink>
        <select name="select" onChange={(e) => setThemeType(e.target.value)}>
          <option value="card">card</option>
          <option value="table">table</option>
        </select>
      </header>
      <div className={styles.HeaderDummy} />
    </>
  );
}

export default Header;
